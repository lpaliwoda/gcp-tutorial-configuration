
## TARGETS
.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: start
start: ## Starts
	minikube start

.PHONY: dashboard
dashboard: ## Enable dashboard
	minikube dashboard --url &

.PHONY: forward-argocd
forward-argocd: ## forward-argocd
	kubectl port-forward service/argocd-server -n argocd 8080:443 &

.PHONY: forward-registry-ui
forward-registry-ui: ## forward-registry-ui
	kubectl port-forward service/registry-service-ui -n registry 8081:8081 &

.PHONY: forward-registry
forward-registry: ## forward-registry
	kubectl port-forward service/registry-service -n registry 5000:5000 &

.PHONY: forward-ports
forward-ports: ## Forward ports
	forward-argocd
	forward-registry
	forward-registry-ui

.PHONY: stop
stop: ## Stops Docker containers
	minikube stop

