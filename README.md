kubectl port-forward service/argocd-server -n argocd 8080:443 &
kubectl port-forward service/registry-service-ui -n registry 8081:8081 &
kubectl port-forward service/registry-service -n registry 5000:5000 &



export GOOGLE_APPLICATION_CREDENTIALS=/home/lukas/.config/gcloud/application_default_credentials.json
minikube addons enable gcp-auth



kubectl create secret docker-registry gcr-json-key \
--docker-server=https://gcr.io \
--docker-username=_json_key \
--docker-password="$(cat ~/Pobrane/nauka-gcp-404410-77159ecf1987.json)" \
--docker-email=luk.paliwoda@gmail.com

kubectl patch serviceaccount gcptutorial -p '{"imagePullSecrets": [{"name": "gcr-json-key"}]}'

kubectl get serviceaccount default -o yaml


kubectl create secret docker-registry gar-reader-2-json-key \
--docker-server=https://gcr.io \
--docker-username=_json_key \
--docker-password="$(cat ~/Pobrane/gar-reader-2-json-key.json)" \
--docker-email=luk.paliwoda@gmail.com


kubectl patch serviceaccount gcptutorial \
          -p '{"imagePullSecrets": [{"name": "gar-reader-2-json-key"}]}'
